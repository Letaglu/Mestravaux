// SPRINT 5
#pragma warning(disable: 4996)
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#define lgrMot 30
#define maxcycliste 100
#define dossardnum 101
#define maxPoints 10

typedef struct{

	char nom[lgrMot + 1];
	char pays[lgrMot + 1];
	unsigned char dossard;

}Cycliste;

typedef struct{

	Cycliste datacycliste[maxcycliste];
	unsigned char taillepeloton;


}Peloton;


typedef struct{

	double temps; //temps chronom�tr�
	int noPoint; // n� du point de chrononom�trage
	// de 0 � maxPoint-1 et -1 pour abandon
	unsigned int dossard; // n� de dossard

}Mesure;

typedef struct{

	Mesure data[maxcycliste*maxPoints]; //temps chronom�tr�s
	unsigned int nbPoints; // nombre de points chronom�trage
	unsigned int taille; // nombre d�enregistrements de mesure

}Course;


void erreur(int typerreur){

	switch (typerreur)
	{
	case 1:
		printf("point de chronometrage ou numero de dossard inexistant\n");
		break;
	case 2:
		printf("temps de chronometrage ne respectant pas l'ordre chronologique\n");

		break;
	case 3:
		printf("discontinuit� dans les points de chronom�trage d'un coureur\n");
		break;
	case 4:
		printf("demande d'un classement au point de d�part\n");
		break;
	}
}


void definir_course(Course* crs){

	char mots[lgrMot + 1];
	scanf("%s", &mots);
	crs->nbPoints = atoi(mots);
}


void enregistrer_temps(Course* crs){

	int error = 1, testerreur = 0, maxdossard = 0;
	char tps = 0;
	char mot[lgrMot + 1];
	scanf("%s", &mot);

	crs->data[crs->taille].temps = atof(mot);
	scanf("%s", &mot);
	testerreur = atoi(mot);
	if (testerreur<-1 || testerreur>crs->nbPoints)
	{
		erreur(error);
		return;
	}
	crs->data[crs->taille].noPoint = atoi(mot);
	scanf("%s", &mot);
	testerreur = atoi(mot);
	maxdossard = crs->data[crs->taille].dossard;
	if (testerreur<dossardnum || testerreur> maxdossard)
	{
		erreur(error);
		return;
	}
	crs->data[crs->taille].dossard = atoi(mot);

}

void afficher_abandon(const Peloton* plt, const Course* crs){

	unsigned int i, doss = 0, j, imin, dossn[maxcycliste], compteur = 0, echange = 0;
	for (i = 0; i < crs->taille; i++)
	{
		if (crs->data[i].noPoint < 0)
		{
			doss = crs->data[i].dossard - dossardnum;
			dossn[compteur] = doss;
			compteur += 1;
		}
	}
	for (i = 0; i < compteur; i++)  //trier par ordre de dossard pour les abandons
	{
		imin = i;
		for (j = i + 1; j <= compteur; j++)
		{
			if (dossn[j] < dossn[imin])
			{
				imin = j;
			}

		}
		if (imin != i)
		{
			echange = dossn[imin];
			dossn[imin] = dossn[i];
			dossn[i] = echange;
		}
	}
	for (i = 0; i < compteur; i++)
	{
		printf("abandon %d %s %s\n", plt->datacycliste[i].dossard, plt->datacycliste[i].nom, plt->datacycliste[i].pays);
	}
}

void detection_fin_course(const Peloton* plt, const Course* crs){

	int compteur = 0, max = 0, doss = dossardnum, pointmax = 0;
	unsigned int i;
	pointmax = crs->nbPoints - 1;
	max = plt->datacycliste[plt->taillepeloton - 1].dossard;
	for (i = 0; i < crs->taille; i++)
	{
		if (crs->data[i].noPoint < 0)
		{
			max -= 1;
		}
		if (crs->data[i].noPoint == pointmax)
		{
			doss += 1;
		}

	}
	if (doss == max)
	{
		printf("detection_fin_course\n");
	}
}

void afficher_temps(const Course* crs){
	unsigned int doss;
	unsigned int i;
	scanf("%d", &doss);
	for (i = 0; i <= crs->taille; i++)
	{
		if (crs->data[i].dossard == doss)
		{
			printf("%d %d %3.1lf \n", crs->data[i].noPoint, doss, crs->data[i].temps);
		}
	}
}


void afficher_classement(const Peloton* plt, const Course* crs){
	unsigned int tDos[maxcycliste];
	double tTps[maxcycliste], echangefl = 0;
	unsigned int point, i, j, imin, echange = 0, cptr = 0, cptr2 = 0, doss = 0, numtab = 0;
	scanf("%d", &point);
	for (i = 0; i < crs->taille; ++i)
	{
		if (crs->data[i].noPoint == point)
		{

			for (j = 0; j < crs->taille; ++j){

				if ((crs->data[i].dossard == crs->data[j].dossard) && (crs->data[j].noPoint == 0)) {

					tTps[cptr] = crs->data[i].temps - crs->data[j].temps;
					tDos[cptr] = crs->data[i].dossard;
					cptr++;
				}
			}
		}
	}
	for (i = 0; i < cptr; i++)
	{
		imin = i;
		for (j = i + 1; j <= cptr; j++)
		{
			if (tTps[j] < tTps[imin])
			{
				imin = j;
			}

		}
		if (imin != i)
		{
			echangefl = tTps[imin];       // on �change les val grace a la var echange
			tTps[imin] = tTps[i];
			tTps[i] = echangefl;
			echange = tDos[imin];
			tDos[imin] = tDos[i];
			tDos[i] = echange;
		}
	}
	for (i = 1; i <= cptr; i++)
	{
		numtab = tDos[i] - dossardnum;
		printf("%d %d %s %s %3.1lf \n", point, tDos[i], plt->datacycliste[numtab].nom, plt->datacycliste[numtab].pays, tTps[i]);
	}


}

void inscription_coureur(Peloton* plt) {
	char mot[lgrMot + 1];
	scanf("%s", &mot);
	strcpy(plt->datacycliste[plt->taillepeloton].nom, mot);
	scanf("%s", &mot);
	strcpy(plt->datacycliste[plt->taillepeloton].pays, mot);
	plt->datacycliste[plt->taillepeloton].dossard = dossardnum + plt->taillepeloton;
	printf("inscription dossard %d \n", plt->datacycliste[plt->taillepeloton].dossard);
	plt->taillepeloton++;

}

void affichage_peloton(const Peloton* plt) {

	int i = 0;
	while (i<   plt->taillepeloton)
	{
		printf("%s %s %d\n", plt->datacycliste[i].nom, plt->datacycliste[i].pays, plt->datacycliste[i].dossard);
		i = i + 1;

	}
}



int main() {
	unsigned char  bouclenumber = 0;
	unsigned char instruction[lgrMot + 1];
	Peloton plt;
	Course crs;
	plt.taillepeloton = 0;
	crs.taille = 0;


	do {

		scanf("%s", &instruction);

		if (strcmp(instruction, "inscrire_coureur") == 0)
		{

			inscription_coureur(&plt);

		}
		else if (strcmp(instruction, "afficher_peloton") == 0)
		{

			affichage_peloton(&plt);

		}

		else if (strcmp(instruction, "definir_course") == 0)
		{

			definir_course(&crs);

		}
		else if (strcmp(instruction, "enregistrer_temps") == 0)
		{

			enregistrer_temps(&crs);
			crs.taille++;
			detection_fin_course(&plt, &crs);
		}
		else if (strcmp(instruction, "afficher_temps") == 0)
		{

			afficher_temps(&crs);

		}
		else if (strcmp(instruction, "afficher_classement") == 0)
		{

			afficher_classement(&plt, &crs);

		}

		else if (strcmp(instruction, "exit") == 0)
		{
			afficher_abandon(&plt, &crs);
			exit(0);
		}

	} while (1);

	system("pause"); return 0;
}